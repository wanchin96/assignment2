import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validator, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  contactForm: FormGroup;
  submitted : boolean = false;
  constructor() { }

  ngOnInit() {
    this.contactForm = new FormGroup ({
      username: new FormControl(null, [Validators.required, Validators.minLength(3), this.blankSpaces]),
      useremail   : new FormControl(null, [Validators.required, Validators.email]),
      usermessage : new FormControl(null, [Validators.required, this.blankSpaces, Validators.pattern("(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}")])
    });
  }

  onSubmit(){
    console.log(this.contactForm);
    this.submitted = true;
  }
  reset(){
    this.submitted = false;
    this.contactForm.reset({
      username : "",
      useremail : "",
      usermessage : ""
    });
  }
  blankSpaces(control: FormControl) : {[s: string] : boolean} {
    if (control.value != null && control.value.trim().length === 0) {
      return {blankSpaces: true};
    }
    return null;
  }

}
