import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service'

export interface DialogData {
  username: string;
  password: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService : AuthService) { }

  ngOnInit() {
  }

  onLogout(){
    this.authService.logout();
  }
}
