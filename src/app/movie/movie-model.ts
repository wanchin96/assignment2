export class Movie{
    // public title: string;
    // public actor: string;
    // public studio: string;
    // public year: string;
    // public genre: string;

    // constructor(title: string, actor:string, studio:string, year:string, genre:string){
    //     this.title = title;
    //     this.actor = actor;
    //     this.studio = studio;
    //     this.year = year;
    //     this.genre = genre;
    // }

    constructor(public id: number, public title: string, public actor:string, public studio:string, public year:string, public genre:string, public genreDesc:string, public imgUrl:string){
    }
    
}