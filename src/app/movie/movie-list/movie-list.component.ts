import { Component, OnInit, Input, EventEmitter, Output  } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Movie } from '../movie-model';
import { MoviesService } from '../movies.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  
  @Input() allMovie : Movie[];
  constructor(public moviesService: MoviesService, private router: Router,
    private route: ActivatedRoute) { }
  genreAll : any;
  disableTxt : any;

  searchStr: string = "";
  seachField : string = "";
  genre: string = "";

  ngOnInit() {
    var genreList = ["Actions", "Horror", "Funny"];
    // for(var i = 0; i < this.allMovie.length; i++){
    //   genreList.push(this.allMovie[i].genre);
      
    // }
    this.genreAll = genreList;
    this.disableTxt = 'disabled';
    // console.log(this.genreAll);
  }

  onSelectType(typeVal){
    if(typeVal == ""){
      this.disableTxt = 'disabled';
    }else{
      this.disableTxt = null;
      
    }
  }

  onSelect(genreVal){
    this.moviesService.selectGenre(genreVal);
  }

  onViewDetail(id : number){
    this.router.navigate(['/movie', id]);
  }
}
