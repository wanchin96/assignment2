import { Component, OnInit } from '@angular/core';
import { Movie } from './movie-model';
import { MoviesService} from './movies.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css'],
  providers: [MoviesService]
})

export class MovieComponent implements OnInit {

  movieList : Movie[] = [];
  constructor(private moviesService : MoviesService) { 
  }

  ngOnInit() {
    //this.movieList = this.moviesService.getMovie();
    this.moviesService.loadMovie().subscribe((result)=>{
      this.movieList = this.moviesService.getMovie();
    });

    this.moviesService.movieAdded.subscribe(()=>{
      this.movieList = this.moviesService.getMovie();
    });
    
    this.moviesService.movieFilter.subscribe(()=>{
      this.movieList = this.moviesService.getMovie();
    });
    this.moviesService.movieAdded.subscribe(()=>{
      this.movieList = this.moviesService.getMovie();
    });
    //this
    
  }
 
  onMovieAdded(newMovieInfo){
    this.movieList.push(newMovieInfo);
    //console.log(this.movieList);
  }
}
