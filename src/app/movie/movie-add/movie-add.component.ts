import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, Input } from '@angular/core';
import { Movie} from '../movie-model';
import { MoviesService } from '../movies.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-movie-add',
  templateUrl: './movie-add.component.html',
  styleUrls: ['./movie-add.component.css']
})
export class MovieAddComponent implements OnInit {
  

  @Output() movieAdded = new EventEmitter<Movie>();
  @Input() allMovie : Movie[];

  constructor(public moviesService: MoviesService, private router: Router, private routes: ActivatedRoute ) { }
  id: any;  
  showUpdate : any
  showAdd : any
  selectedMovie: Movie = new Movie(0, "", "", "", "", "", "", "");

  ngOnInit() {

    this.id = this.routes.snapshot.params['id'];
    if(this.id == undefined){
      this.showAdd = true;
      this.showUpdate = false;

    }else{
      this.showUpdate = true;
      this.showAdd = false;
      this.selectedMovie = this.moviesService.getMovieDetails(this.id);
      
      // if(this.selectedMovie === undefined){
      //   this.router.navigate(['not-found']);
      // }
    }
  }
 

  

  onAddMovie(title: HTMLInputElement, actor: HTMLInputElement, studio: HTMLInputElement, year:HTMLInputElement, genre:HTMLInputElement, genreDesc:HTMLInputElement, imgUrl:HTMLInputElement){
    //console.log(title.value);
    this.moviesService.addMovie(new Movie(
      0, title.value, actor.value, studio.value, year.value, genre.value, genreDesc.value, imgUrl.value
    ));
  }
}
