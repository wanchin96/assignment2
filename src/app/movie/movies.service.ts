import { Injectable, EventEmitter } from '@angular/core';
import {  Movie} from './movie-model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  movieAdded = new EventEmitter<void>();
  movieFilter = new EventEmitter<void>();
 
  private movieList : Movie[] = [];
    // new Movie(1,'Avengers End Game', 'Chris Evan', 'Marvel', '2019', 'Actions', ' actions movie', 'http:movie/avengers.com'),
    // new Movie(2,'The Nun', 'Patrick Wilson', 'James Wan', '2018', 'Horror', 'Horror movie', ''),
    // new Movie(3,'Central Intelligence', 'Dwayne Johnson', 'Calvin Joyner', '2016', 'Funny', ' Funny movie', ''),
    // new Movie(4,'Harry Potter', 'James Potter', 'Rowli Ng J.K.', '2001', 'Funny', ' Thriller movie', '')
  
 
  constructor(public httpClient : HttpClient) { }

  loadMovie(){
    return this.httpClient.get<Movie []>("http://3.217.84.185:3000/api/movieList")
    .pipe(
      map((movies) => {
        this.movieList = movies;
        return movies;
      },
      (error)=> {console.log(error);}
      )
    )
  }

  addMovie(newMovieInfo) {
    const httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'Cache-Control' : 'no-cache'
    });

    const options = {
      headers : httpHeaders
    }

    this.httpClient.post<Movie>("http://3.217.84.185:3000/api/movie",
    {info: newMovieInfo}, options)
    .subscribe((res)=>{
      this.movieList.push(res);
      this.movieAdded.emit();
    })

    //console.log(newMovieInfo);
    // this.movieList.push(newMovieInfo);
    // this.movieAdded.emit();
  }
 
  getMovie(){
    return this.movieList.slice();
  }

  defaultList:any;
  selectGenre(val){
    this.defaultList = this.movieList;
    this.movieList = this.defaultList.filter(x => x.genre == val)
    this.movieFilter.emit();
  }

  getMovieDetails(movieId : number){
    
    const result = this.movieList.find((elem) => {
      return (elem.id == movieId);
    });
    console.log(this.movieList);
    return result;    
  }
  
}
