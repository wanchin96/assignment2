import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'find'
})
export class FindPipe implements PipeTransform {

  transform(value: any, propertyName : string, searchStr : string): any {
    if(value.length === 0 || searchStr.length === 0){
      return value;
    }
    let resultArray = [];
    searchStr = searchStr.toLocaleLowerCase();
    for(const x of value){
      const str = x[propertyName].toLocaleLowerCase();
      if(str.indexOf(searchStr) != -1){
        resultArray.push(x);
      }
    }
    return resultArray;
  }

}
