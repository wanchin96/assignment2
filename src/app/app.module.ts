import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './auth/auth.service';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MovieComponent } from './movie/movie.component';
import { MovieAddComponent } from './movie/movie-add/movie-add.component';
import { MovieListComponent } from './movie/movie-list/movie-list.component';
import { HoverHighlightDirective } from './hover-highlight.directive';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRountingModule } from './app-rounting.module';
import { FindPipe } from './find.pipe';
import { LoginComponent} from './login/login.component'


@NgModule({
  declarations: [
    AppComponent,
    MovieComponent,
    MovieAddComponent,
    MovieListComponent,
    HoverHighlightDirective,
    ContactUsComponent,
    HomeComponent,
    HeaderComponent,
    PageNotFoundComponent,
    FindPipe,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRountingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
