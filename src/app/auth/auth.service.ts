import { Router } from "@angular/router";
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedIn = false;
  constructor(private router : Router) { }

  login(username: string, password: string){
    this.loggedIn = (username == "ABC" && password == "123");
    if(this.loggedIn){
      this.router.navigate(["/movie"]);
    }
  }

  logout(){
    this.loggedIn = false;
    this.router.navigate(["/"]);
  }
}
