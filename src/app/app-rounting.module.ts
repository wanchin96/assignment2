import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule} from '@angular/router'

import { HomeComponent } from './home/home.component';
import { MovieComponent } from './movie/movie.component';
import { MovieAddComponent } from './movie/movie-add/movie-add.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component'

const appRoutes: Routes = [
  {path: '', redirectTo:'/home', pathMatch: 'full'},
  {path: 'login', component:LoginComponent},
  {path: 'home', component:HomeComponent},
  {path: 'movie', component:MovieComponent},
  {path: 'new', component:MovieAddComponent},
  {path: 'movie/:id', component:MovieAddComponent},
  {path: 'contactus', component:ContactUsComponent},
  {path: 'not-found', component:PageNotFoundComponent},
  {path: '**', redirectTo:'/not-found'}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRountingModule { }
